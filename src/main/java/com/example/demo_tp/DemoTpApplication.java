package com.example.demo_tp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTpApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTpApplication.class, args);
	}

}
